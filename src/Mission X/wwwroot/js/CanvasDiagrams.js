﻿var x, y, z, x1, y1, z1,a1;
var link, canvasId, filename;
var scale = 1;
function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

/** 
 * The event handler for the link's onclick event. We give THIS as a
 * parameter (=the link element), ID of the canvas and a filename.
*/
function down() {
    document.getElementById('download').addEventListener('click', function () {
        downloadCanvas(this, 'Simple_Canvas', 'Simpleproblem.png');
    }, false);
}
function downMultiple() {
    document.getElementById('download2').addEventListener('click', function () {
        downloadCanvas(this, 'Multiple_Canvas', 'Multipleproblem.png');
    }, false);
}
function downComplex() {
    document.getElementById('download3').addEventListener('click', function () {
        downloadCanvas(this, 'Complex_Canvas', 'Complexproblem.png');
    }, false);
}

function CanvasDiagram(x, y, z) {
    var height = x;
    var width = y;
    
   
    var e = document.getElementById("Distance");
    var canvasElement = document.querySelector("#Simple_Canvas");
    var context = canvasElement.getContext("2d");
    context.font = '16pt Times new roman';
    context.fillStyle = 'red';
    if (height > width) {
        scale = 300 / height;
       
    }
    if (height < width) {
        scale = 300 / width;
       
    }
    context.beginPath();
   
    context.moveTo(50, 50);

    context.lineTo(50, 50 + x*scale);
    context.lineTo(50 + y * scale, 50 + x * scale);
    context.closePath();
    context.lineWidth = 1.7;
    context.strokeStyle = 'Black';
    context.stroke();
    context.fillStyle = "Yellow";
    context.fill();
    context.fillStyle = 'red';
    context.fillText('c = ' + x + e.options[e.selectedIndex].value, 0, 199);
    context.fillText('a = ' + y + e.options[e.selectedIndex].value, 125, 330);
    context.fillText('b = ' + z + e.options[e.selectedIndex].value, 215, 199);
    alert(scale);
}
function onlyAngles(x,z)
{
    var e = document.getElementById("Distance");
    var canvasElement = document.querySelector("#Simple_Canvas");
    var context = canvasElement.getContext("2d");
    context.font = '16pt Times new roman';
    context.fillStyle = 'red';
     y = 90;
    context.fillText('∠A = ' + x + '°', 85, 89);
    context.fillText('∠B = ' + y + '°', 15, 300);
    context.fillText('∠C = ' + z + '°', 315, 299);
    context.beginPath();
    context.moveTo(120, 120);
    context.lineTo(120, 300);
    context.lineTo(300, 300);
    context.closePath();
    context.lineWidth = 1.7;
    context.strokeStyle = 'Black';
    context.stroke();
    context.fillStyle = "yellow";
    context.fill();
}
function sidesAndAngles(x,z,x1,y1,z1)
{
    var e = document.getElementById("Distance");

    var canvasElement = document.querySelector("#Simple_Canvas");
    var context = canvasElement.getContext("2d");
    context.font = '16pt Times new roman';
    context.fillStyle = 'red';
    y = 90;
    context.fillText('∠A = ' + x + '°', 85, 89);
    context.fillText('∠B = ' + y + '°', 15, 300);
    context.fillText('∠C = ' + z + '°', 315, 299);
    context.fillText('c = ' + x1 + e.options[e.selectedIndex].value, 25, 199);
    context.fillText('a = ' + y1 + e.options[e.selectedIndex].value, 125, 330);
    context.fillText('b = ' + z1 + e.options[e.selectedIndex].value, 215, 199);
    context.beginPath();
    context.moveTo(120, 120);
    context.lineTo(120, 300);
    context.lineTo(300, 300);
    context.closePath();
    context.lineWidth = 1.7;
    context.strokeStyle = 'Black';
    context.stroke();
    context.fillStyle = "yellow";
    context.fill();
}
function ComplexDiagram(x,y,z,x1,y1,z1)
{
    var e = document.getElementById("Distance2");
    var midpointAX, midpointAY, midpointBX, midpointBY, midpointCX,midpointCY;
    var canvasElement = document.querySelector("#Complex_Canvas");
    var context = canvasElement.getContext("2d");
    if (x1 < z1 * Math.cos(y * 0.0174533))
    {
        scale = 300 / x1;
    }
    if (x1 > z1 * Math.cos(y * 0.0174533)) {
        scale = 300 / z1 * Math.cos(y * 0.0174533);
    }
    midpointBX = ((50 + (z1 * Math.cos(y * 0.0174533)) * scale) + (50 + x1 * scale)) / 2;
    midpointBY = (((350 - (z1 * Math.sin(y * 0.0174533)) * scale) - 20) + 360) / 2;
    midpointAX = (50 + (50 + x1 * scale)) / 2;
    midpointAY = (360 + 370) / 2;
    midpointCX = ((50 + (z1 * Math.cos(y * 0.0174533)) * scale) + 50) / 2;
    midpointCY = (((350 - (z1 * Math.sin(y * 0.0174533)) * scale) - 20) + 380) / 2;
    context.beginPath();
    context.moveTo(50, 350);
    context.lineTo(50+x1 * scale, 350);
    context.lineTo(50 + (z1 * Math.cos(y * 0.0174533)) * scale, 350 - (z1 * Math.sin(y * 0.0174533)) * scale);
    context.closePath();

    context.lineWidth = 2.7;
    context.strokeStyle = 'black';
    context.stroke();
    context.fillStyle = "yellow";
    context.fill();
    context.font = '16pt Times new roman';
    context.fillStyle = 'red';
    context.fillText('∠B = ' + y + '°', 30, 390);
    context.fillText('c = ' + z1 + e.options[e.selectedIndex].value,midpointCX - 50,midpointCY);
    context.fillText('a = ' + x1 + e.options[e.selectedIndex].value, midpointAX, midpointAY+10);
    context.fillText('∠C = ' + z + '°',  (50 + x1 * scale) + 20, 380);
    context.fillText('b = ' + y1 + e.options[e.selectedIndex].value, midpointBX, midpointBY);
    context.fillText('∠A = ' + x + '°', 50 + (z1 * Math.cos(y * 0.0174533)) * scale, (350 - (z1 * Math.sin(y * 0.0174533)) * scale) - 20);
    alert(scale);
}
function ComplexCanvasDiagram(x,y,z,x1,a1)
{
    var e = document.getElementById("Distance1");
    var canvasElement = document.querySelector("#Multiple_Canvas");
    var context = canvasElement.getContext("2d");
    context.font = '16pt Times new roman';
    context.fillStyle = 'red';
    context.fillText('c = ' + x + e.options[e.selectedIndex].value, 5, 199);
    context.fillText('b = ' + x + e.options[e.selectedIndex].value, 240, 170);
    context.fillText('d = ' + y + e.options[e.selectedIndex].value, 125, 280);
    context.fillText('e = ' + z + e.options[e.selectedIndex].value, 240, 280);
    context.fillText('∠C = ' + x1 + '°', 335, 280);
    context.fillText('∠A = ' + a1 + '°', 1, 70);
    context.fillText('∠B = 90' + '°', 40, 280)

    context.beginPath();
    context.moveTo(100, 100);
    context.lineTo(100, 250);
    context.lineTo(330, 250);
    context.lineTo(220, 250);
    context.lineTo(100, 100);
    context.lineTo(330, 250);

    context.lineWidth = 1.7;
    context.strokeStyle = 'black';
    context.stroke();
    context.fillStyle = "yellow";

    context.fill();
}

function clearDiagram()
{
    var canvasElement = document.querySelector("#Simple_Canvas");
    var context = canvasElement.getContext("2d");
    context.clearRect(0, 0, canvasElement.width, canvasElement.height);
}
function clearDiagram2() {
    var canvasElement = document.querySelector("#Multiple_Canvas");
    var context = canvasElement.getContext("2d");
    context.clearRect(0, 0, canvasElement.width, canvasElement.height);
}
function clearDiagram3() {
    var canvasElement = document.querySelector("#Complex_Canvas");
    var context = canvasElement.getContext("2d");
    context.clearRect(0, 0, canvasElement.width, canvasElement.height);
}