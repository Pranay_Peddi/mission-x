﻿
var strikeAngle = 0;

function calcu() {
    // Retrieving the values 
    var point = document.getElementById("pointA").value;
    var point1 = document.getElementById("pointB").value;
    var point2 = document.getElementById("pointC").value;
    var dista = document.getElementById("dist").value;
    var distb = document.getElementById("dist1").value;
    var beara = document.getElementById("bear1").value;
    var bearb = document.getElementById("bear2").value;
    //validations
    if(point =="" || null){
        alert("The value cannot be empty");
        return false;
    }
    if (point1 == "" || null) {
        alert("The value cannot be empty");
        return false;
    }
    if (point2 == "" || null) {
        alert("The value cannot be empty");
        return false;
    }
    if (dista == "" || null) {
        alert("The value cannot be empty");
        return false;
    }
    if (distb == "" || null) {
        alert("The value cannot be empty");
        return false;
    }
    if (beara == "" || null) {
        alert("The value cannot be empty");
        return false;
    }
    if (bearb == "" || null) {
        alert("The value cannot be empty");
        return false;
    }
    if(point==point1 || point==point2 || point1==point2 ){
        alert("The elevations cannot be same");
        return false;
    }
    if (beara > 360 || bearb > 360) {
        alert("The angle cannot be greater than 360");
        return false;
    }
    if (beara == bearb) {
        alert("The angles cannot be same");
        return false;
    }
    
    // finding angle of strike
    var temp = point1 - point2;
    var temp1 = point - point2;
    var diff = temp1 * 100 / temp;
    diff = diff.toFixed(1);
    //document.getElementById('msg1').innerHTML = diff;
    if (360<beara<270 && 270<bearb<180) {
        var temp2 = beara - 270;
        var tem = Math.tan(temp2 * Math.PI / 180.0);
        tem = tem.toFixed(1);
        var side = 50 /tem;
        var ang = diff - 50;
        var angl = ang / side;
        angl = angl.toFixed(2);
       //document.getElementById('msg1').innerHTML = angl;
        var angle = Math.atan(angl);
        angle =90+(angle * 180.0) / Math.PI;
        document.getElementById('msg1').innerHTML = angle;
        strikeAngle = angle;
    }
    else if(270<beara<180 && 180 <bearb<90){
        var temp2 = beara - 180;
        var tem = Math.tan(temp2 * Math.PI / 180.0);
        tem = tem.toFixed(1);
        var side = 50 / tem;
        var ang = diff - 50;
        var angl = ang / side;
        angl = angl.toFixed(2);
        //document.getElementById('msg1').innerHTML = angl;
        var angle = Math.atan(angl);
        angle = 180 + (angle * 180.0) / Math.PI;
        document.getElementById('msg1').innerHTML = angle;
        
        strikeAngle = angle;
    }
    else if (180 < beara < 90 && 0 < bearb < 90) {
        var temp2 = beara - 90;
        var tem = Math.tan(temp2 * Math.PI / 180.0);
        tem = tem.toFixed(1);
        var side = 50 / tem;
        var ang = diff - 50;
        var angl = ang / side;
        angl = angl.toFixed(2);
        //document.getElementById('msg1').innerHTML = angl;
        var angle = Math.atan(angl);
        angle = 180 + (angle * 180.0) / Math.PI;
        document.getElementById('msg1').innerHTML = angle;
        strikeAngle = angle;
    }
    else if (90 < beara < 0 && 360 < bearb < 270) {
        var temp2 = beara - 90;
        var tem = Math.tan(temp2 * Math.PI / 180.0);
        tem = tem.toFixed(1);
        var side = 50 / tem;
        var ang = diff - 50;
        var angl = ang / side;
        angl = angl.toFixed(2);
        //document.getElementById('msg1').innerHTML = angl;
        var angle = Math.atan(angl);
        angle = 180 + (angle * 180.0) / Math.PI;
        document.getElementById('msg1').innerHTML = angle;
        strikeAngle = angle;
    }
    var dip = point1 / point;
    dip = dip.toFixed(2);
    dip = Math.atan(dip);
    var dipa = (dip * 180.0) / Math.PI;
    document.getElementById('msg4').innerHTML = dipa;
    param(strikeAngle);
}
