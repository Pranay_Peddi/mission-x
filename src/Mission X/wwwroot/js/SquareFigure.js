﻿function generateSquareFigure(angle, dipdirection) {
    var canvas = document.getElementById('my_Canvas');
    var gl = canvas.getContext('experimental-webgl');
    var radians = Number(angle) * (Math.PI / 180);
    var y_cordinate = Math.sin(radians);
    var x_cordinate = Math.cos(radians);

    var x_dippoint = 0.0;
    var y_dippoint = 0.0;

    var dipdirection = "NE";
    if (dipdirection == "NE") {
        x_dippoint = x_cordinate;
        y_dippoint = y_cordinate;
    }

    else if (dipdirection == "NW") {
        x_dippoint = -x_cordinate;
        y_dippoint = y_cordinate;
    }
    else if (dipdirection == "SW") {
        x_dippoint = -x_cordinate;
        y_dippoint = -y_cordinate;
    }
    else if (dipdirection == "SE") {
        x_dippoint = x_cordinate;
        y_dippoint = -y_cordinate;
    }
    else {
        window.alert("invalid dipdirection");
    }


    /*======= Defining and storing the geometry ======*/

    var vertices = [
       -1.0, 0.0, 0,
       1.0, 0.0, 0,
       0.0, -1.0, 0,
       0.0, 1.0, 0,

       x_cordinate / 1.5, y_cordinate / 1.5, 0,
       -x_cordinate / 1.5, -y_cordinate / 1.5, 0,


       x_cordinate / 7, y_cordinate / 7, 0,
       (x_cordinate / 7) + 0.15, (y_cordinate / 7) - 0.15, 0,


       -x_cordinate / 7, -y_cordinate / 7, 0,
       (-x_cordinate / 7) + 0.15, (-y_cordinate / 7) - 0.15, 0,

       (x_cordinate / 7) + 0.15, (y_cordinate / 7) - 0.15, 0,
       (-x_cordinate / 7) + 0.15, (-y_cordinate / 7) - 0.15, 0,

    ]

    // Create an empty buffer object
    var vertex_buffer = gl.createBuffer();

    // Bind appropriate array buffer to it
    gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);

    // Pass the vertex data to the buffer
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

    // Unbind the buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    /*=================== Shaders ====================*/

    // Vertex shader source code
    var vertCode =
       'attribute vec4 coordinates;' +
       'uniform mat4 u_xformMatrix;' +
       'void main(void) {' +
          '  gl_Position = u_xformMatrix * coordinates;' +
       '}';

    // Create a vertex shader object
    var vertShader = gl.createShader(gl.VERTEX_SHADER);

    // Attach vertex shader source code
    gl.shaderSource(vertShader, vertCode);

    // Compile the vertex shader
    gl.compileShader(vertShader);

    // Fragment shader source code
    var fragCode =
       'void main(void) {' +
          'gl_FragColor = vec4(0,0,0,0.1);' +
       '}';

    // Create fragment shader object
    var fragShader = gl.createShader(gl.FRAGMENT_SHADER);

    // Attach fragment shader source code
    gl.shaderSource(fragShader, fragCode);

    // Compile the fragmentt shader
    gl.compileShader(fragShader);

    // Create a shader program object to store
    // the combined shader program
    var shaderProgram = gl.createProgram();

    // Attach a vertex shader
    gl.attachShader(shaderProgram, vertShader);

    // Attach a fragment shader
    gl.attachShader(shaderProgram, fragShader);

    // Link both the programs
    gl.linkProgram(shaderProgram);

    // Use the combined shader program object
    gl.useProgram(shaderProgram);

    /*======= Associating shaders to buffer objects ======*/
    var Sx = 1.4, Sy = 1.4, Sz = 1.0;
    var xformMatrix = new Float32Array([
       Sx, 0.0, 0.0, 0.0,
       0.0, Sy, 0.0, 0.0,
       0.0, 0.0, Sz, 0.0,
       0.0, 0.0, 0.0, 1.0
    ]);

    var u_xformMatrix = gl.getUniformLocation(shaderProgram, 'u_xformMatrix');
    gl.uniformMatrix4fv(u_xformMatrix, false, xformMatrix);

    // Bind vertex buffer object
    gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);

    // Get the attribute location
    var coord = gl.getAttribLocation(shaderProgram, "coordinates");

    // Point an attribute to the currently bound VBO
    gl.vertexAttribPointer(coord, 3, gl.FLOAT, false, 0, 0);

    // Enable the attribute
    gl.enableVertexAttribArray(coord);

    /*============ Drawing the triangle =============*/

    // Clear the canvas
    gl.clearColor(0.5, 0.5, 0.5, 0.9);

    // Enable the depth test
    gl.enable(gl.DEPTH_TEST);

    // Clear the color and depth buffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Set the view port
    gl.viewport(0, 0, canvas.width, canvas.height);

    // Draw the triangle
    gl.drawArrays(gl.LINES, 0, 12);

    // POINTS, LINE_STRIP, LINE_LOOP, LINES,
    // TRIANGLE_STRIP,TRIANGLE_FAN, TRIANGLES

}